﻿using OpenWeatherMap.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherMap
{
    public class Response
    {
        public FullWeatherObject Forecast { get; set; }

        public ErrorMessage Message { get; set; }        
    }
}
