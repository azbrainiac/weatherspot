﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherMap.DataObjects
{
    public class TemperatureItem
    {
        public double Day { get; set; }

        public double Min { get; set; }

        public double Max { get; set; }

        public double Night { get; set; }

        public double Eve { get; set; }

        public double Morning { get; set; }
    }
}
