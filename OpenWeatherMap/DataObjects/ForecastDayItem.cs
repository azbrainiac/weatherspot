﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherMap.DataObjects
{
    public class ForecastDayItem
    {
        public int Clouds { get; set; }

        public int Deg { get; set; }

        public int Dt { get; set; }

        public int Humidity { get; set; }

        public double Pressure { get; set; }

        public double Snow { get; set; }

        public double Speed { get; set; }

        [JsonProperty("temp")]
        public TemperatureItem Temperature { get; set; }

        public IEnumerable<WeatherItem> Weather { get; set; }
    }
}
