﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherMap.DataObjects
{
    public class ErrorMessage
    {
        public string Cod { get; set; }

        public string Message { get; set; }
    }
}
