﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherMap.DataObjects
{
    public class FullWeatherObject
    {
        public City City { get; set; }

        public int Cnt { get; set; }

        public string Cod { get; set; }

        public string Message { get; set; }

        public IEnumerable<ForecastDayItem> List { get; set; }

    }
}
