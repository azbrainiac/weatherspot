﻿using Newtonsoft.Json;
using OpenWeatherMap.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OpenWeatherMap
{
    public class Receiver
    {
        private const string QUERY_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?q={0}&mode=json&units=metric&cnt={1}&apikey={2}";

        private Response _response;
        
        public Response Response
        {
            get
            {
                return _response;
            }
        }                

        private HttpStatusCode _responseStatus;
        public HttpStatusCode ResponseStatus
        {
            get
            {
                return _responseStatus;
            }
        }
        private readonly string _currentQueryUrl;
        


        public Receiver(string cityName, string apiKey, int countOfDays = 3)
        {            
            _currentQueryUrl = string.Format(QUERY_URL, cityName, countOfDays, apiKey);            
        }

        public void GetData()
        {
            _response = new Response();
            using(var client = new HttpClient())
            {
                var response = client.GetAsync(_currentQueryUrl);
                _responseStatus = response.Result.StatusCode;
                var result = response.Result.Content.ReadAsStringAsync().Result;
                switch (_responseStatus)
                {
                    case HttpStatusCode.OK:
                        _response.Forecast = JsonConvert.DeserializeObject<FullWeatherObject>(result);
                        break;
                    default:
                        _response.Message = JsonConvert.DeserializeObject<ErrorMessage>(result);
                        break;
                }
            }            
        }

        
    }
}
