﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeatherSpot.Web.Controllers;
using System.Web.Mvc;
using WeatherSpot.Web.Models;
using Moq;
using System.Web;

namespace WeatherSpot.Web.Tests
{
    [TestClass]
    public class HomeControllerTest
    {
        private HomeController controller;
        private ViewResult indexViewResult;               

        [TestInitialize]
        public void Setup()
        {
            var mockHttpContext = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var response = new Mock<HttpResponseBase>();
            var cookie = new HttpCookieCollection();
            request.SetupGet(x => x.Cookies).Returns(cookie);
            response.SetupGet(x => x.Cookies).Returns(cookie);
            mockHttpContext.SetupGet(x => x.Request).Returns(request.Object);
            mockHttpContext.SetupGet(x => x.Response).Returns(response.Object);
            controller = new HomeController()
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = mockHttpContext.Object
                }
            };            
            indexViewResult = controller.Index(null) as ViewResult;
        }

        [TestMethod]
        public void IndexViewResultNotNull()
        {                        
            Assert.IsNotNull(indexViewResult);
        }        

        [TestMethod]
        public void GetForecastViewResultIsNotNull()
        {
            PartialViewResult result = controller.GetForecast(null, null) as PartialViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetForecastViewEqualForecastError()
        {
            PartialViewResult result = controller.GetForecast(null, null) as PartialViewResult;
            Assert.AreEqual("ForecastErrorPartial", result.ViewName);
        }

        [TestMethod]
        public void GetForecastViewEqualForecast()
        {
            
            PartialViewResult result = controller.GetForecast("Kiev", new RecentCities()) as PartialViewResult;
            Assert.AreEqual("ForecastPartial", result.ViewName);
        }

        [TestMethod]
        public void GetForecastViewModelEqualForecast()
        {
            PartialViewResult result = controller.GetForecast("Kiev", new RecentCities()) as PartialViewResult;
            Assert.IsInstanceOfType(result.Model, typeof(ForecastViewModel));
        }

        [TestMethod]
        public void GetForecastCookies()
        {
            PartialViewResult result = controller.GetForecast("Kiev", new RecentCities()) as PartialViewResult;

            Assert.IsNotNull(controller.Response.Cookies["recentCities"]);
            Assert.AreEqual(controller.Response.Cookies["recentCities"].Value, "Kiev");
        }

    }
}
