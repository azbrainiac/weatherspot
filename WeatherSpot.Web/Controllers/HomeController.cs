﻿using OpenWeatherMap;
using OpenWeatherMap.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeatherSpot.Web.Models;
using WeatherSpot.Web.Models.Mappers;

namespace WeatherSpot.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(RecentCities model)
        {
            return View(model);
        }
        
        [HttpPost]
        [OutputCache(Duration = 3600, Location = System.Web.UI.OutputCacheLocation.Server, VaryByParam = "query")]
        public ActionResult GetForecast(string query, RecentCities cities)
        {
            if (!string.IsNullOrEmpty(query))
            {
                var forecastReceiver = new Receiver(query, "94b7c9a9908d65670675b7edd72079fc");
                forecastReceiver.GetData();
                var response = forecastReceiver.Response;
                if (forecastReceiver.ResponseStatus == System.Net.HttpStatusCode.OK)
                {
                    cities.Add(query);
                    var cookie = Request.Cookies["recentCities"] == null ? new HttpCookie("recentCities") : Request.Cookies["recentCities"];
                    cookie.Value = string.Join(",", cities.Cities.ToArray());
                    Response.Cookies.Add(cookie);
                    ViewBag.recentCities = cities;
                    return PartialView("ForecastPartial",  ForecastMapper.ToViewModel(response.Forecast));
                }
                else
                {

                    return PartialView("ForecastErrorPartial", response.Message);
                }
            }
            return PartialView("ForecastErrorPartial", new ErrorMessage { Message = "Query must not be empty!" });
        }
    }
}