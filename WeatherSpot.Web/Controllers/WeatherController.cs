﻿using OpenWeatherMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using WeatherSpot.Web.Infrastructure;
using WeatherSpot.Web.Models.Mappers;
using WeatherSpot.Web.Models.WebAPI;

namespace WeatherSpot.Web.Controllers
{
    [CustomAuthorize]
    public class WeatherController : ApiController
    {

        private ForecastCache _forecastCache;

        public WeatherController()
        {
            _forecastCache = new ForecastCache();
        }

        public IHttpActionResult GetDaily(string city, int count = 3)
        {
            var forecastModel = _forecastCache.GetValue(city, count);
            if (forecastModel == null)
            {
                var forecastReceiver = new Receiver(city, "94b7c9a9908d65670675b7edd72079fc", count);
                forecastReceiver.GetData();
                var response = forecastReceiver.Response;
                if (forecastReceiver.ResponseStatus == System.Net.HttpStatusCode.OK)
                {
                    forecastModel = ForecastWebAPIMapper.ToModel(response.Forecast);
                    _forecastCache.Add(forecastModel);                    
                }
                else
                {
                    return BadRequest(response.Message.Cod + ": " + response.Message.Message);
                }
            }
            return Ok(forecastModel);            
        }       
        
        public IHttpActionResult GetToday(string cities)
        {
            if (string.IsNullOrEmpty(cities))
            {
                return BadRequest();
            }
            SeveralCitiesForecast sevCitiesForecast = new SeveralCitiesForecast();
            sevCitiesForecast.TakeAll(cities);            
            return Ok(sevCitiesForecast.Models);
        }
        
    }
}
