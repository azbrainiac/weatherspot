﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using WeatherSpot.Web.Models.WebAPI;

namespace WeatherSpot.Web.Infrastructure
{
    public class ForecastCache
    {
        public ForecastModel GetValue(string cityName, int count)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            string id = cityName + count.ToString();
            return memoryCache.Get(id) as ForecastModel;
        }

        public bool Add(ForecastModel modelToCache)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Add(modelToCache.Location.Name + modelToCache.Forecast.Count(), modelToCache, DateTime.Now.AddHours(1));
        }
    }
}