﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace WeatherSpot.Web.Infrastructure
{
    public class CustomAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string _apiKey;

        public CustomAuthorizeAttribute()
        {
            var appSettings = ConfigurationManager.AppSettings;
            _apiKey = appSettings["WeatherApiKey"]; 
        }

        public bool AllowMultiple
        {
            get
            {
                return false;
            }
        }

        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            if (!actionContext.Request.Headers.Contains("Api-Key"))
            {
                return Task.FromResult<HttpResponseMessage>(actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized));
            }
            var apiKeyHeader = actionContext.Request.Headers.GetValues("Api-Key");
            if (apiKeyHeader == null || apiKeyHeader.Any(val => val != _apiKey))
            {
                return Task.FromResult<HttpResponseMessage>(actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized));
            }
            else
            {
                return continuation();
            }
        }
    }
}