﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeatherSpot.Web.Models;

namespace WeatherSpot.Web.Infrastructure
{
    public class RecentCitiesBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = new RecentCities();
            string citiesString = controllerContext.RequestContext.HttpContext.Request.Cookies != null && controllerContext.RequestContext.HttpContext.Request.Cookies["recentCities"] != null ? controllerContext.RequestContext.HttpContext.Request.Cookies["recentCities"].Value : null;
            if(!string.IsNullOrEmpty(citiesString))
            {
                foreach(var cityName in citiesString.Split(','))
                {
                    model.Add(cityName);
                }                
            }
            return model;
        }
    }
}