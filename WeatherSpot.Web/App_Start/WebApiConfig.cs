﻿using Newtonsoft.Json.Serialization;
using System.Web.Http;

namespace WeatherSpot.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Daily",
                routeTemplate: "api/weather/daily/{city}",
                defaults: new { controller = "Weather", action = "GetDaily" }
            );

            config.Routes.MapHttpRoute(
                name: "Today",
                routeTemplate: "api/weather/today",
                defaults: new { controller = "Weather", action = "GetToday" }
            );            

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
