﻿using OpenWeatherMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using WeatherSpot.Web.Models.Mappers;

namespace WeatherSpot.Web.Models.WebAPI
{
    public class SeveralCitiesForecast
    {
        private List<ForecastModel> _models = new List<ForecastModel>();

        public IEnumerable<ForecastModel> Models
        {
            get
            {
                return _models;
            }
        }

        private void Set(object city)
        {
            var forecastReceiver = new Receiver((string)city, "94b7c9a9908d65670675b7edd72079fc", 1);
            forecastReceiver.GetData();
            var response = forecastReceiver.Response;
            if (forecastReceiver.ResponseStatus == System.Net.HttpStatusCode.OK)
            {
                var forecastModel = ForecastWebAPIMapper.ToModel(response.Forecast);
                _models.Add(forecastModel);
            }

        }

        private IEnumerable<string> TakeCities(string citiesString)
        {
            return citiesString.Split(',');
        }

        public void TakeAll(string citiesString)
        {
            foreach (string city in TakeCities(citiesString))
            {
                ParameterizedThreadStart set = new ParameterizedThreadStart(Set);
                Thread thread = new Thread(set);
                thread.Start(city);
                thread.Join();
            }
        }

    }
}