﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models.WebAPI
{
    public class LocationModel
    {
        public string Name { get; set; }

        public string Country { get; set; }
    }
}