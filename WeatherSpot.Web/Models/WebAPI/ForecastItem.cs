﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models.WebAPI
{
    public class ForecastItem
    {
        public string Date { get; set; }

        public string Summary { get; set; }

        [JsonProperty(PropertyName = "temp")]
        public Temperature Tempetature { get; set; }

        public Wind Wind { get; set; }
    }

    public class Temperature
    {
        public double Day { get; set; }

        public double Night { get; set; }

    }

    public class Wind
    {
        public int Direction { get; set; }

        public double Speed { get; set; }
    }
}