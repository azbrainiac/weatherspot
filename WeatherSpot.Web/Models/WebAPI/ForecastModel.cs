﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models.WebAPI
{
    public class ForecastModel
    {
        public LocationModel Location { get; set; }

        public IEnumerable<ForecastItem> Forecast { get; set; }
    }
}