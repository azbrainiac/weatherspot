﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models
{
    public class RecentCities
    {
        private Queue<string> _cities = new Queue<string>();
        public Queue<string> Cities
        {
            get
            {
                return _cities ?? new Queue<string>();
            }            
        }

        public void Add(string name)
        {
            if(!_cities.Contains(name))
            {
                _cities.Enqueue(name);
            }
        }
    }
}