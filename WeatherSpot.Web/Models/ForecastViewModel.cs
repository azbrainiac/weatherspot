﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models
{
    public class ForecastViewModel
    {
        public string City { get; set; }

        public IEnumerable<ForecastItemViewModel> ForecastItems { get; set; }
    }
}