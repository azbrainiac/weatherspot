﻿using OpenWeatherMap.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherSpot.Web.Models.WebAPI;

namespace WeatherSpot.Web.Models.Mappers
{
    public class ForecastWebAPIMapper
    {
        public static ForecastModel ToModel(FullWeatherObject weatherObject)
        {
            ForecastModel viewModel = new ForecastModel();
            viewModel.Forecast = weatherObject.List.Select(ToItem);
            viewModel.Location = new LocationModel
            {
                Country = weatherObject.City.Country,
                Name = weatherObject.City.Name
            };
            return viewModel;
        }

        private static ForecastItem ToItem(ForecastDayItem forecastDayItem)
        {
            return new ForecastItem
            {
                Date = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(forecastDayItem.Dt).ToString("yyyy-MM-dd"),
                Summary = forecastDayItem.Weather.First().Description,
                Tempetature = new Temperature
                {
                    Day = forecastDayItem.Temperature.Day,
                    Night = forecastDayItem.Temperature.Night
                },
                Wind = new Wind
                {
                    Direction = forecastDayItem.Deg,
                    Speed = forecastDayItem.Speed
                }
            };
        }
    }
}