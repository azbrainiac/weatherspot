﻿using OpenWeatherMap.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models.Mappers
{
    public class ForecastMapper
    {
        public static ForecastViewModel ToViewModel(FullWeatherObject weatherObject)
        {
            ForecastViewModel viewModel = new ForecastViewModel();
            viewModel.ForecastItems = weatherObject.List.Select(ToItemViewModel);
            viewModel.City = weatherObject.City.Name;
            return viewModel;
        }

        private static ForecastItemViewModel ToItemViewModel(ForecastDayItem forecastDayItem)
        {
            return new ForecastItemViewModel
            {
                Date = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(forecastDayItem.Dt),
                Description = forecastDayItem.Weather.First().Description,
                MaxTemperature = forecastDayItem.Temperature.Max,
                MinTemperature = forecastDayItem.Temperature.Min,
                WindAngle = forecastDayItem.Deg,
                WindSpeed = forecastDayItem.Speed
            };
        }

    }

    
}