﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherSpot.Web.Models
{
    public class ForecastItemViewModel
    {
        public DateTime Date { get; set; }

        public string Description { get; set; }

        public double MinTemperature { get; set; }

        public double MaxTemperature { get; set; }

        public double WindSpeed { get; set; }

        public int WindAngle { get; set; }
    }
}